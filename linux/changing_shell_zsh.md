# How to change your default shell to Zsh (debian)
1. Install the zsh package 
	`sudo apt install -y zsh`
2. Change shell 
	`chsh -s /usr/bin/zsh`
3. Logout or exit shell session
	`exit`
4. Log back in / open up a new terminal session
5. Confirm shell has been updated
	`echo $0`
6. Add 'Oh-My-Zsh!' extension for more customizable shell
	`sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"`


